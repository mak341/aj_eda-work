% Script used to convert lables from an excel file into a vector to be used
% for comparison with sparse recovery method. 
%
% Last modified 2/24/2017
% Written by: Malia Kelsey

clear
clc
close all;

%get directory with data
p = uigetdir;
cd(p)

%load the master labels file **based on file from Ian **
Numbers = xlsread('SCR_List_All_PPs_reworked.xlsx');
%extract needed columns 
ppnums = Numbers(:,1);
picnums = Numbers(:,2);
label1 = Numbers(:,6);
label2 = Numbers(:,7);

% clear extra data
clear Numbers

path = strcat(p,'\EDA_Data');
cd(path)

%find all files to load
event_files = dir('Data_Events_*');
event_files = extractfield(event_files,'name');

%loop through all the files format
for i = 1:length(event_files)
    name = event_files{i};
    load(name);
    
    %get the patient ID in the form PP**
    patientID = strsplit(event_files{i},'_');
    patientID = patientID(3);
    patientID = strsplit(patientID{1},'.');
    patientID = char(patientID(1));
    
    %Extract patient number from patientID
    patnum = strsplit(patientID,'PP');
    patnum = str2num(char(patnum(2)));
    
    range = find(ppnums==patnum);
    
    % check to see if there are labels in that range
    test = isempty(range);
    if test == 0
        alpha = min(range);
        omega = max(range);
        
        number_scr_4sec = label1(alpha:omega);
        number_scr_full = label2(alpha:omega);
        
        %append new labels into the Label struct
        Labels.SCRs_4sec = number_scr_4sec;
        Labels.SCRs_full = number_scr_full;
        
        %save
        save(name,'Labels','-append')
    end
end
% load in the AJ data and the image events and save into mat files

clear
clc
close all;

%get directory with data
p = uigetdir;
cd(p)

%find all files to load
event_files = dir('*-ImagesOnly.txt');
event_files = extractfield(event_files,'name');

%loop through all the files that need to  be formatted
for i = 1:length(event_files)

    patientID = strsplit(event_files{i},'-');
    patientID = patientID(4);
    data_file = strcat('Data-AJ-EDA-Text-',patientID,'-S1.txt');
    
    Events = tdfread(event_files{i});
    Data = tdfread(data_file{1});
    
    %rename fields in the data struct
    Data.Time = Data.Time_0x28s0x29;
    Data.EDA = Data.GSC_Ch4;
    Data = rmfield(Data,'Time_0x28s0x29');
    Data = rmfield(Data,'GSC_Ch4');
    
    %% manipulate the events data (code from Ian)
    %----------------------------------------------------------
    % Number of events
    Nevents = length(Events.Time);
    
    % Process each event
    event_Fkey        = cell(1,Nevents);
    event_name        = cell(1,Nevents);
    event_time_hrs = zeros(1,Nevents);
    
    for k_event = 1:Nevents
        % Get Fkey and name of each event
        event_Fkey{k_event} = deblank( Events.Event_Type(k_event,:) );
        event_name{k_event} = deblank( Events.Name(k_event,:) );
        
        % Get time from string like "09:34:06.578 AM"
        event_time_string = Events.Time(k_event,:);
        event_time_num_array = sscanf(event_time_string, '%02d:%02d:%f %s');
        
        hrs  = event_time_num_array(1);
        mins = event_time_num_array(2);
        secs = event_time_num_array(3);
        AMPM_string = sprintf('%s%s',event_time_num_array(4), event_time_num_array(5));
        
        event_time_hrs(k_event) = hrs + mins/60 + secs/60/60;
        
        % 1PM and later is 12 hrs into the day
        if( strcmp(AMPM_string, 'PM') && hrs ~= 12 )
            event_time_hrs(k_event) = 12 + event_time_hrs(k_event);
            
            % 12AM is hour zero
        elseif( strcmp(AMPM_string, 'AM') && hrs == 12 )
            event_time_hrs(k_event) = event_time_hrs(k_event) - 12;
        end
    end
    
    % Convert hours to other units
    event_time_min = event_time_hrs*60;
    event_time_sec = event_time_hrs*60*60;
    %----------------------------------------------------------
    
    %save event times into struct
    Events.Time_sec = event_time_sec;
    Events.Time_min = event_time_min;
    Events.Time_hrs = event_time_hrs;
    
    %% manipulate EDA data
    init_freq = 1000; %Hz
    %change frequency as desired (between 1 - 3 Hz)
    new_freq = 1; %Hz
    
    tic
    %load prebuilt filter
    load('0p5HzLPFilter_1000Hz.mat');
    
    %apply filter and remove sidelobes
    eda_filt=conv(Num, Data.EDA);
    eda_filt=eda_filt(ceil((length(Num)/2)+1): end-ceil(length(Num)/2+1));
    
    %find ratio for downsample
    [p, q] = rat(new_freq/init_freq);
    
    %downsample
    EDA_down = resample(eda_filt,p,q);
    Data.EDA = EDA_down;
    
    %make sure time matches
    Time = (0:(length(EDA_down)-1))*q/(p*init_freq);
    Data.Time = Time';
    Data.Fs = new_freq;
    
    t_down = toc;
    
    Data.downsamp_time = t_down;
    
    %remove tonic our method
    %create struct to save data in
    phasic_ours = struct;
    %distance allowed between minimums (values 1-10)
    len = 1; 
    tic
    [phasic_ours.phasicData, phasic_ours.tonicData] = Phasic_estimation2(EDA_down,Data.Time,new_freq,len);
    t_ours = toc;
    
    phasic_ours.process_time = t_ours + t_down;
    phasic_ours.Fs = new_freq;
    
    %save phasic into Data struct
    Data.phasic_ours = phasic_ours;
    
    %% save everything needed
    name = strcat('Data_Events_p5Filt', char(patientID(1)));
    save(name,'Data','Events')
end

This code can be used to complete sparse recovery to identify SCRs within a signal. The included files are as follows: 

Tbased_B_OMP_AJ: this is the main file that can be used to complete analysis of files 
create_dictionary: function called from main file to create a dictionary to be used in sparse recovery 
dictionary: function called from create_dictionary 
TOMP_AJ: function called from main file to complete batch orthogonal matching pursuit
event_performance: function called from the main script to complete performance analysis 
----------------------------------------------------------------------------------------------------------------
Code needed for analysis that wasn't written by me 
OMPbox10: code to complete the OMP analysis
citation: R. Rubinstein, M. Zibulevsky and M. Elad, "Efficient Implementation of the K-SVD Algorithm using 
	  Batch Orthogonal Matching Pursuit," CS Technion, vol. 40, no. 8, pp. 1-15, 2008.

----------------------------------------------------------------------------------------------------------------
Additional items: 
load_data: script that can be used to load data and create mat files to be used in further analysis. This script also
	   completes some of the preprocessing steps
generate_labels_new: used to create a label vector based on excel files provided 
phasic_Estimation2: function called by load_data and main script. Used to complete our phasic estimation and return
	             the estimated phasic and tonic signals
time_threshold: function called by phasic_Estimation2 
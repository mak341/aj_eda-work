%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main script which can be used to loop through various files and complete% 
% SCR identification uses sparse recovery with a predefined dictionary    %
% Last modified 2/24/2017                                                 %
% Written By: Malia Kelsey                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
clear;
clc;

% get root folder for files 
p = uigetdir;
%cd(p)

%get path and move to that folder 
path = strcat(p,'\test');
%path = strcat(p,'\EDA_Data\Run_3');
cd(path)

% get number of files and save file names for loop
d = dir('Data_Events_*');
scrList_files = extractfield(d,'name');

%set up T values (desired sparsity levels) to loop through during BOMP
T_f = [10 25 50 75 100 150 200 250 300 350 400]';

% go through each file and do calculations
for i = 1:length(scrList_files)
    %extract name from file for later use (mostly for saving pursposes)
    name = scrList_files{i};
    name = strsplit(name,'_');
    name = strsplit(name{3},'.');
    name = char(name(1));
    
    %load file
    load(scrList_files{i})
    %check that we have labels for loaded file
    check = exist('Labels','var');
    if check == 0  %will skip files we don't have labels for
        continue
    end
    
    %% create dictionary for file
    % this section is all user defined and can be expaned and shrunk as
    % necassary to fit analysis. Currently using 7 different SCR shapes to
    % create final dictionary. Note: the SCR length is based on the length
    % of time required for the SCR to recovery given the tau1 & tau2 pair
    
    %define variable necassary to create dictionary 
    t = Data.Time';
    
    %1st defined tau1 and tau2 pair
    tau1 = .75;
    tau2 = 20;
    scr_len = 30; %seconds
    
    %Run dictionary creation function
    [dictionary1, time1_1] = create_dictionary(tau1,tau2,t,scr_len,Data.Fs);
    
    %2nd defined tau1 and tau2 pair
    tau1 = 1.71;
    tau2 = 1.72;
    scr_len = 10; %seconds
    
    %Run dictionary creation function
    [dictionary2, time1_2] = create_dictionary(tau1,tau2,t,scr_len,Data.Fs);
    
    %3rd defined tau1 and tau2 pair
    tau1 = 5.83;
    tau2 = 5.84;
    scr_len = 20;
    
    %Run dictionary creation function
    [dictionary3, time1_3] = create_dictionary(tau1,tau2,t,scr_len,Data.Fs);
    
    %4th defined tau1 and tau2 pair
    tau1 = 1.175;
    tau2 = 1.176;
    scr_len = 5; %seconds
    
    %Run dictionary creation function
    [dictionary4, time1_4] = create_dictionary(tau1,tau2,t,scr_len,Data.Fs);
    
    %5th defined tau1 and tau2 pair
    tau1 = 1.852;
    tau2 = 1.853;
    scr_len = 7; %seconds
    
    %Run dictionary creation function
    [dictionary5, time1_5] = create_dictionary(tau1,tau2,t,scr_len,Data.Fs);
    
    %6th defined tau1 and tau2 pair
    tau1 = 2.258;
    tau2 = 2.259;
    scr_len = 10; %seconds
    
    %Run dictionary creation function
    [dictionary6, time1_6] = create_dictionary(tau1,tau2,t,scr_len,Data.Fs);
    
    %7th defined tau1 and tau2 pair
    tau1 = .02;
    tau2 = 2.678;
    scr_len = 5; %seconds
    
    %Run dictionary creation function
    [dictionary7, time1_7] = create_dictionary(tau1,tau2,t,scr_len,Data.Fs);
    
    %combine separate dictionaries into final dictionary to be used by OMP
    dictionary = [dictionary1 dictionary2 dictionary3 dictionary4 dictionary5 dictionary6 dictionary7];
    time1 = time1_1 + time1_2 + time1_3 + time1_4 + time1_5 + time1_6 + time1_7;
    
    %for memory and performace clear unnecessary variables  
    clear dictionary1 dictionary2 dictionary3 dictionary4 dictionary5 dictionary6 dictionary7 time1_1 time1_2 time1_3 time1_4 time1_5 time1_6 time1_7
    
    %% General preprocessing/ signal clean up 
    
    %create a phasic estimate using a different threshold value
    % note: this step can also be done during the loading of the data 
    phasic_ours = struct;
    %set threshold
    phasic_ours.Threshold = 1;
    tic
    [phasic_ours.phasicData, phasic_ours.tonicData] = Phasic_estimation2(Data.EDA,Data.Time,Data.Fs,phasic_ours.Threshold);
    t_ours = toc;
    
    phasic_ours.process_time = t_ours;
    
    %save phasic into Data struct
    Data.phasic_ours_new2 = phasic_ours;
    
    %trim the beg and end of the file to remove noise based on when first
    %event occured
    phasic = Data.phasic_ours_new2.phasicData;
    beg_cut = Events.Time_sec(2) - Events.Time_sec(1);
    beg_cut = beg_cut - 2;
    if beg_cut >= 0
        diff_cut = Data.Time - beg_cut;
        [~, cut_idx] = min(abs(diff_cut));
        phasic(1:cut_idx) = 0;
    end
    len = length(Events.Time_sec);
    end_cut = Events.Time_sec(len) - Events.Time_sec(1);
    end_cut = end_cut + 12;
    if end_cut <= length(Data.Time)
        diff_cut = Data.Time - end_cut;
        [~, cut_idx] = min(abs(diff_cut));
        phasic(cut_idx:end) = 0;
    end
    
    %% loop through each T value for the file
    for j = 1:length(T_f)
        %set sparsity value
        T = T_f(j);
        
        %Function to complete OMP analysis using BOMP code
        [Gamma,time2] = TOMP_AJ(phasic,T,dictionary);
        time_TOMP = time1 + time2;
        time_tot = time_TOMP + Data.phasic_ours_new2.process_time;
        
        %Create Estimate of signal
        Est = full(dictionary*Gamma);
        
        %Threshold gamma values - remove negative gamma values
        Gamma_Full = full(Gamma);
        thresh_gamma = zeros(size(Gamma_Full));
        thresh_gamma(Gamma_Full > 0) = 1;
        gamma_thresh = sparse(Gamma_Full.*thresh_gamma);
        
        %Generate new Estimated signal based on new gamma values
        Est_threshold = full(dictionary*gamma_thresh);
        
        %clear unneed variables
        clear Gamma_Full thresh_gamma 
        
        %count number of labeled SCRs in file
        N_SCRs = sum(Labels.SCRs_full,'omitnan');
        
        %time range to look at from -1 sec to +8 (same range evaluated during labeling) 
        start_time = -1;
        end_time = 9;
        
        %calculate performance based off of provided event labels
        %performance based on no thresholding (negative values still in gamma)
        Calc_No_Threshold = event_performance(Data.Time, Events.Time_sec, Labels.SCRs_full, Gamma, start_time, end_time);
        %performance based on thresholded gamma values
        Calc_Threshold = event_performance(Data.Time, Events.Time_sec, Labels.SCRs_full, gamma_thresh, start_time, end_time);
        
        
        %save file 
        file = strcat('TOMP_',num2str(T),'_',name,'_phasic_ours_new3.mat');
        save(file,'Labels','Data','Events','N_SCRs','Gamma','gamma_thresh','Est','Est_threshold','time1','time2','time_TOMP','time_tot','Calc_No_Threshold','Calc_Threshold','dictionary','-v7.3')
        
        %clear large variables before next analysis is completed
        clear Est Est_threshold; clear Gamma gamma_thresh; clear est_labels; clear est_oncet_idx;clear Gamma_Thresh;
    end
    %clear large variables before next analysis 
    clear dictionary; clear Data; clear Events; clear Labels
end
disp('Done!')
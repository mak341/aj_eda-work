function [Calc] = event_performance(time, event, labels, my_gamma, scr_start, event_length)
%function to calculate performance measures based on provided events and
%lables
%
% This function loops through each event and determines what was labled in
% that event and what was captured by sparse recovery
%
% INPUT: time- time vector
%        event- event vector
%        labels- labels corresponding to each event
%        my_gamma- gamma returned from OMP 
%        scr_start- start time for potental scr
%        event_length- length of event section to look in
% OUTPUT: Calc- structure with each performance count and full performance
%         measures 
%
% Last modified 2/24/2017
% Written by: Malia Kelsey




%normalize event times to first event
event = event - event(1);
len = length(time);


%zero performance labels
TP = 0;
FP = 0;
TN = 0;
FN = 0;
%create vectors for later review
misses = [];
extra = [];

%loop through each event and determine performace label 
for i = 2:length(event)
   %start and end time
   event_start = event(i) + scr_start;
   event_end = event_start + event_length;
   
   %find index
   diff_start = time - event_start;
   diff_end = time - event_end;
   [~, start_idx] = min(abs(diff_start));
   [~, end_idx] = min(abs(diff_end));
   
   %onset of OMP dictionary actaully 1 - column 
   start_idx = start_idx + 1;
   
   %determine # of SCRs detected 
   %Note: this will change based on # of tau1 and tau2 pairs used
   %(currently based on 7 parameter pairs)
   SCRs_id = find(my_gamma(start_idx:end_idx)~=0 | my_gamma(start_idx + len:end_idx+len)~=0 | my_gamma(start_idx+len*2:end_idx+len*2)~=0 | my_gamma(start_idx+len*3:end_idx+len*3)~=0 | my_gamma(start_idx+len*4:end_idx+len*4)~=0 | my_gamma(start_idx+len*5:end_idx+len*5)~=0 | my_gamma(start_idx+len*6:end_idx+len*6)~=0); %TOMP analysis
   
   %threshold onsets within desired range from each other -- will be
   %dependent on desire and sampeling frequency
   if isempty(SCRs_id) ~= 1 && length(SCRs_id) > 1
       diffSCR = diff(SCRs_id);
       scrFlag = zeros(length(diffSCR),1);
       scrFlag(diffSCR > 1) = 1;
       num_scrs = sum(scrFlag) + 1;
   else 
       num_scrs = length(SCRs_id);
   end
   
   %check what the "true" label is 
   true_label = labels(i-1);
   if isnan(true_label) 
      %do nothing because I wasn't sure how to interpret some labels/ they
      %didn't fall into the analysis I was trying to complete
       continue
   end
   diff_nums = true_label - num_scrs;
   
   %determine if SCR was detected/if there should be one
   if true_label == 0 && num_scrs == 0 
       TN = TN + 1;
   elseif diff_nums == 0
       TP = TP + num_scrs;
   elseif diff_nums > 0
       TP = TP + num_scrs;
       FN = FN + diff_nums;
       misses = [misses; [start_idx end_idx true_label num_scrs]];
   elseif diff_nums < 0 
       TP = TP + true_label;
       FP = FP + abs(diff_nums);
       extra = [extra; [start_idx end_idx true_label num_scrs]];
   end
end

%calculate performance measures
%sensitivity (True positive rate)
TPR = TP/(TP + FN);
%Specificity SPC (True Negative Rate)
SPC = TN/(TN + FP);
%Precision (positive predictive value)
PPV = TP/(TP + FP);
%Negative Predictive Value
NPV = TN/(TN + FN);
%Accuracy 
ACC = (TP + TN)/ (TP + FP + FN + TN);

%save all information into a struct
Calc = struct('True_Neg',TN,'False_Neg',FN,'True_Pos',TP,'False_Pos',FP,'Sensitivity',TPR,'Specificity',SPC,'Percision',PPV,'Neg_Predictive_value',NPV,'Accuracy',ACC,'Misses',misses,'Extras',extra);

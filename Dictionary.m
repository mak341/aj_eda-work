function x = Dictionary(t,t1,t2,tau1,tau2,sr)
%function to create signle dictionary column
%
% This function builds a single dictionary column where the SCR falls
% between t1 and t2 and has a shape created by the bateman function with
% specified tau1 and tau2 parameters
%
% Last modified 2/24/2017
% Written by: Malia Kelsey

len1 = t2 - t1;
%for some sampling frequencies (3 Hz) I had to add a fudge factor due to
%how matlab rounds 1/3 in that case replace line 11 with:
%len1 = (t2 - t1) + .0001;

t3 = (1/sr):(1/sr):(len1);

x(t<t1) = 0;

x(t1<=t & t<t2) = (exp(-t3./tau2) - exp(-t3./tau1));

x(t>=t2) = 0;

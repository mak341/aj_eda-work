function [dictionary, time1]  = create_dictionary(tau1, tau2, time, SCRlength,sr)
%function to create dictionary with size being length by length
%
% This function builds the dictionary were each column represents 1 SCR
% (given the specific Tau1 and Tau2 values) at different points in time.
% Each column is shifted in time by 1 sample (i.e for 1 Hz the first column
% SCR has an onset at 0 sec and then second column SCR has an onset at 1 sec
%
% Last modified 2/24/2017
% Written by: Malia Kelsey

%this will be the size of the matrix (matches the length of the EDA data)
len = length(time);

%create dictionary based on tau values and bateman equation
D = zeros(len,len);
t1 = 0; %set time onset 

%for some sampling frequencies (3 Hz) I had to add a fudge factor due to
%how matlab rounds 1/3 in that case add:
%t = t+.001

tic
for i = 1:len
    %check for the end of the signal
    if t1+ SCRlength <= (len*(1/sr))
        t2 = t1+SCRlength;
    else
        t2 = (len*(1/sr));
    end
    %create signle column with 1 SCR
    x = Dictionary(time,t1,t2,tau1,tau2,sr);
    x = x';
    %normalize data and add to dictionary 
    x = normc(x);
    D(:,i) = x;
    t1 = t1 + 1/sr; %shift onset by 1 sample
end
time1 = toc; %record how long it takes to create each dictionary 

%make dictionary sparse to pass back
dictionary = sparse(D);
function [phasic_Est, tonicData_est2] = Phasic_estimation2(data,time,Fs,scrLength)
% Estimation of phasic component
% Ahmed Dallal
% Oct 2015
% last modified by: Malia Kelsey
% on 2/24/2017

params.LMtype = 'LocalMin'; % extraction prameters
params.LMdefn = 'strict';

%time = data.time;
timeOffset=time(1);
timeNorm=time-time(1);

eda_filt = data;

[indExt,valExt] = findLocalExtrema(eda_filt,params);
t_extrim = timeNorm(indExt);


tonicData_est1 = interp1(t_extrim,valExt,timeNorm); 

% Time windowing
% SCR_TimeLength = 3; % (Seconds) this can be edited fro different lenthes of SCR
SCR_TimeLength = scrLength; %*Fs; % (Seconds) this can be edited for different lenthes of SCR
% 
[t_extrim_filtered, idx] = Time_Threshold(t_extrim,SCR_TimeLength);
tonicData_est2 = interp1(t_extrim_filtered,valExt(idx),timeNorm);
tonicData_est2(isnan(tonicData_est2)) = 0;
phasic_Est = eda_filt - tonicData_est2;


function  [Gamma,time2] = TOMP_AJ(analysis,T,D)
%function to complete BOMP analysis 
%
% Input: Phasic Data (analyis), Sparsity value (T), and dictionary (D)
% Return: gamma which represents the weights of the selected columns and
%         time required to complete analysis 
%
% Last modified 2/24/2017
% Written by: Malia Kelsey

%create full dictionary
D = full(D);

%Apply batch-OMP algorithm 
tic
Gamma = omp(D,analysis,[],T);
time2 = toc;

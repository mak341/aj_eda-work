function [N_t_extrim,idx] = Time_Threshold(t_extrim,dT)

%This function return the extrima that are dT seconds apart
%%
% INPUTS:
% t_extrim: time corresponding to detected local minimas
% dT: SCR time length / threshold for excluding minimas
% 
% OUTPUTS:
% N_t_extrim: filterd time stamps resulted from thresholding
% idx: index of these time stamps in the original input array
% 
% Ahmed Dallal
% August 2015
%%

N_t_extrim = t_extrim(1) ;
idx = 1;
for i = 2:length(t_extrim)
    if t_extrim(i) - N_t_extrim(end) >= dT
        N_t_extrim(end+1) = t_extrim(i);
        idx(end+1) = i;
    end
end
        


% Copy rights: Ahmed Dallal